import {
  Nullable,
  Record,
} from './types'

import { VALIDATION_ERROR } from './constants'
import { ValidationError } from './errors'
import logger from './logger'

const validateRecord = (record: Record): void => {
  const invalidKeys: string[] = []
  for (const [key, value] of Object.entries(record)) {
    if (!value) {
      invalidKeys.push(key)
    }
  }

  if (invalidKeys.length) {
    throw new ValidationError(`Invalid fields - "${invalidKeys.join(',')}"`)
  }
}

export default (line: string, idx: number) :Nullable<Record> => {
  let record :Nullable<Record> = null
  try {
    const json = JSON.parse(line)
    if (!json) return null

    record = {
      user_id: json.user_id,
      name: json.name,
      latitude: +json.latitude, //coerce to number
      longitude: +json.longitude,
    }
    validateRecord(record)
    return record
  } catch (err) {
    if (err.name === VALIDATION_ERROR) {
      logger.error(`Validation error in line ${idx}, (${err.message})\n`)
    } else {
      logger.error(`Syntax error in line ${idx}, (${err.message})\n`)
    }
  }
  return null
}