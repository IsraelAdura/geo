export type Coordinate = {
  latitude: number
  longitude: number
}

export type Input = {
  path?: string
  url?: string
}

export type User = {
  user_id: number
  name: string
}

export type Record = Coordinate & User

export type Nullable<T> = T | null

