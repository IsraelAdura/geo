import { Coordinate } from './types'
import {
  EARTH_RADIUS,
} from './constants'

const degreeToRadian = (deg: number): number => deg * (Math.PI / 180)

/*
* using the "Haversine formula"
*  a = sin²(Δφ/2) + cos φ1 ⋅ cos φ2 ⋅ sin²(Δλ/2)
*  c = 2 ⋅ arcsin(√a)
*  d = R ⋅ c
* to determine the great-circle distance between two points on a sphere given
* their longitudes and latitudes.
* https://en.wikipedia.org/wiki/Great-circle_distance
*/
export default (
  origin: Coordinate,
  destination: Coordinate,
) => {
  const latDiffRadian = degreeToRadian(destination.latitude - origin.latitude)
  const lngDiffRadian = degreeToRadian(destination.longitude - origin.longitude)

  const originLatRadian = degreeToRadian(origin.latitude)
  const destinationLatRadian = degreeToRadian(destination.latitude)

  const a =
      Math.pow(Math.sin(latDiffRadian / 2), 2)
    + Math.cos(originLatRadian)
    * Math.cos(destinationLatRadian)
    * Math.pow(Math.sin(lngDiffRadian / 2), 2)

  const c = 2 * Math.asin(Math.sqrt(a))
  return EARTH_RADIUS * c // to km
}
