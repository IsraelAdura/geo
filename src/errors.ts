import {
  INVALID_PARAMETER_ERROR,
  VALIDATION_ERROR
} from './constants'

export class ValidationError extends Error {
  constructor(message: string) {
    super(message)
    this.name = VALIDATION_ERROR,
    this.message = message
  }
}

export class InvalidParameterError extends Error {
  constructor(message: string) {
    super(message)
    this.name = INVALID_PARAMETER_ERROR,
    this.message = message
  }
}
