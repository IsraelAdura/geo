import { Nullable } from '../types'
import fetch from 'node-fetch'
import logger from '../logger'

export const readRemoteFile = async (url: string): Promise<Nullable<string>> => {
  let data :Nullable<string> = null
  try {
    const resp = await fetch(url)
    data = await resp.text()
  } catch (err) {
    logger.error(`"Error fetching records from ${url}"`)
  }
  return data
}
