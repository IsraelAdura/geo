import { Nullable } from '../types'
import fs from 'fs/promises'
import logger from '../logger'

export const readLocalFile = async (filePath: string): Promise<Nullable<string>> => {
  let data: Nullable<string> = null
  try {
    data = await fs.readFile(filePath, { encoding: 'utf8' })
  } catch (err) {
    logger.error(err, `Error reading file "${filePath}"`)
  }
  return data
}

export const writeOutput = async (data: string, filePath: string): Promise<void> => {
  try {
    await fs.writeFile(filePath, data)
  } catch (err) {
    logger.error(err, `Error writing data to ${filePath}`)
  }
}
