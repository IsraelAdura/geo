export const VALIDATION_ERROR = 'ValidationError'
export const INVALID_PARAMETER_ERROR = 'InvalidParameterError'

export const EARTH_RADIUS = 6371 // Radius of the earth in km
export const MAX_ACCEPTED_DISTANCE_APART = 100

export const DEFAULT_ORIGIN_LATITUDE = 53.339428
export const DEFAULT_ORIGIN_LONGITUDE = -6.257664

export const DEFAULT_OUTPUT_FILE = 'output.txt'
export const DEFAULT_SORT_KEY = 'user_id'