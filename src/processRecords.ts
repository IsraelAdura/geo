import {
  Coordinate,
  User,
} from './types'

import distance from './distance'
import parseRecord from './parseRecord'

export default (
  lines: string[],
  coordinates: Coordinate,
  maxDistanceApart: number,
): User[] => {
  const records: User[] = []

  for (const [idx, line] of Object.entries(lines)) {
    if (!line) {
      continue
    }

    const record = parseRecord(line, +idx)
    if (!record) continue
    const { name, user_id, latitude, longitude } = record

    const destinationCoord = {
      latitude: latitude,
      longitude: longitude,
    }

    const distanceApart = distance(coordinates, destinationCoord)
    if (distanceApart <= maxDistanceApart) {
      records.push({
        name,
        user_id,
      })
    }
  }
  return records
}
