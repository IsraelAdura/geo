// Singleton logger class for pretty printing logs
class PrettyLogger {
  private static colors = {
    Red: '\u001b[31m',
    Green: '\u001b[32m',
    Orange: '\u001b[33m',
    Blue: '\u001b[34m',
  }

  private static instance: PrettyLogger = new PrettyLogger()

  constructor() {
    if (PrettyLogger.instance) {
      return PrettyLogger.instance
    }
    PrettyLogger.instance = this
  }

  public static getInstance() {
    return PrettyLogger.instance
  }

  log(...args: any[]): void {
    console.log(...args)
  }

  warn(...args: any[]): void {
    console.warn(PrettyLogger.colors.Orange, ...args)
  }

  error(...args: any[]): void {
    console.error(PrettyLogger.colors.Red, ...args)
  }

  info(...args: any[]): void {
    console.info(PrettyLogger.colors.Blue, ...args)
  }

  success(...args: any[]): void {
    console.info(PrettyLogger.colors.Green, ...args)
  }
}

export default PrettyLogger.getInstance()
