import {
  Coordinate,
  Input,
  Nullable,
} from './types'
import {
  DEFAULT_ORIGIN_LATITUDE,
  DEFAULT_ORIGIN_LONGITUDE,
  DEFAULT_OUTPUT_FILE,
  DEFAULT_SORT_KEY,
  MAX_ACCEPTED_DISTANCE_APART,
} from './constants'
import {
  readLocalFile,
  writeOutput,
} from './services/fs'

import { InvalidParameterError } from './errors'
import logger from './logger'
import processRecords from './processRecords'
import { readRemoteFile } from './services/fetch'
import sortRecords from './utils/sort'

export default async (input: Input, coordinates?: Coordinate): Promise<void> => {
  const { path, url } = input
  if (!path && !url) {
    throw new InvalidParameterError('Invalid input URL/JSON')
  }

  if (!coordinates) {
    coordinates = {
      latitude: DEFAULT_ORIGIN_LATITUDE,
      longitude: DEFAULT_ORIGIN_LONGITUDE,
    }
  }

  let data: Nullable<string> = null

  if (path) {
    data = await readLocalFile(path)
  } else if (url) {
    data = await readRemoteFile(url)
  }

  if (!data) {
    logger.error('Couldn\'t get data from input')
    process.exit(1)
  }

  const lines = data.split('\n')
  const recordsWithinDistance = processRecords(
    lines,
    coordinates,
    MAX_ACCEPTED_DISTANCE_APART,
  )

  // sorts in-place
  sortRecords(recordsWithinDistance, DEFAULT_SORT_KEY)
  const recordsJSON = JSON.stringify(recordsWithinDistance, null, 4)
  logger.success(recordsJSON)
  await writeOutput(recordsJSON, DEFAULT_OUTPUT_FILE)
}
