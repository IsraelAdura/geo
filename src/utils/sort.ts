const compareASC = (a: any, b: any) => (a > b ? 1 : b > a ? -1 : 0)

export default (list: any[], sortKey: string): void => {
  list.sort((a, b) => compareASC(a[sortKey], b[sortKey]))
}
