// @ts-nocheck
import {
  DEFAULT_ORIGIN_LATITUDE,
  DEFAULT_ORIGIN_LONGITUDE,
  DEFAULT_OUTPUT_FILE,
} from '../src/constants'
import { readFile, rm } from 'fs/promises'

import { InvalidParameterError } from '../src/errors'
import fs from 'fs'
import main from '../src'

const coordinates = {
  latitude: DEFAULT_ORIGIN_LATITUDE,
  longitude: DEFAULT_ORIGIN_LONGITUDE,
} as const

const FILE_PATH = 'tests/data/files/customers.txt'

const input = {
  path: FILE_PATH,
  // url: 'https://s3.amazonaws.com/intercom-take-home-test/customers.txt',
}
const exit = process.exit

describe('Integration test - main script', () => {
  beforeAll((done) => {
    fs.exists(DEFAULT_OUTPUT_FILE, async (exists) => {
      if (exists) {
        await rm(DEFAULT_OUTPUT_FILE)
      }
      done()
    })
  })

  beforeEach(() => {
    process.exit = jest.fn(() => {
      throw 'mockExit'
    })
  })

  it('should fail if no input', async () => {
    await expect(main({}, coordinates)).rejects.toThrow(InvalidParameterError)
  })

  it('should fail for bad file path', async () => {
    try {
      await main({ path: '/tests/not-exists.txt' }, coordinates)
    } catch (err) {
      expect(err).toBe('mockExit')
      expect(process.exit).toBeCalledWith(1)
    }
  })

  it('should fail for bad link', async () => {
     try {
      await main({ url: 'https://example.com/no-exists.txt' }, coordinates)
     } catch (err) {
       expect(err).toBe('mockExit')
       expect(process.exit).toBeCalledWith(1)
     }
  })

  it ('should pass for valid json and create output file', async () => {
    await main(input, coordinates)
    const data = await readFile(DEFAULT_OUTPUT_FILE, 'utf8')
    expect(!!data).toBe(true)
  })

  afterAll(() => {
    process.exit = exit
  })
})
