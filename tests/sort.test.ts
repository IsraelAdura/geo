// @ts-nocheck
import { cloneList as clone } from './utils'
import customersList from './data/customersList'
import sort from '../src/utils/sort'

describe('sort list', () => {
  it('should sort correctly, by "user_id" in ASC order', () => {
    const list1 = clone(customersList)
    sort(list1, 'user_id')
    expect(list1).toMatchSnapshot()
  })

  it('should sort correctly, by "name" in ASC order', () => {
     const list1 = clone(customersList)
     sort(list1, 'name')
     expect(list1).toMatchSnapshot()
  })
})
