//@ts-nocheck
import { readRemoteFile } from '../src/services/fetch'

const FILE_URL = 'https://s3.amazonaws.com/intercom-take-home-test/customers.txt'

describe('fetch remote file', () => {
  it(`should fetch data from remote ${FILE_URL}`, async () => {
    const data = await readRemoteFile(FILE_URL)
    expect(data).toMatchSnapshot()
  })
})
