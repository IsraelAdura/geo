// @ts-nocheck
import {
    readLocalFile,
    writeOutput,
} from '../src/services/fs'

import fs from 'fs/promises'

const FILE_PATH = 'tests/data/files/customers.txt'
const FILE_OUPUT = 'tests/demo.txt'

describe('fs utils', () => {
  it('readLocalFile should read data from file correctly', async () => {
    expect(await readLocalFile(FILE_PATH)).toMatchSnapshot('customer.txt')
  })

  it('writes Output & reads', async () => {
    const text = 'HELLO WORLD'
    await writeOutput(text, FILE_OUPUT)
    expect(await readLocalFile(FILE_OUPUT)).toEqual(text)
  })

  afterAll(() => {
    fs.rm(FILE_OUPUT)
  })
})

