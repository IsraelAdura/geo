// @ts-nocheck
import {
  DEFAULT_ORIGIN_LATITUDE,
  DEFAULT_ORIGIN_LONGITUDE,
} from '../src/constants'

import customersList from './data/customersList'
import distance from '../src/distance'

const origin = {
  latitude: DEFAULT_ORIGIN_LATITUDE,
  longitude: DEFAULT_ORIGIN_LONGITUDE,
}

const data = [
  {
    origin: {
      latitude: 53.339428,
      longitude: -6.257664,
    },
    destination: {
      latitude: 52.3191841,
      longitude: -8.5072391,
    },
    result: 188.95936393870778, //rounded to 3d.p
  },
  {
    origin: {
      latitude: 63.339428,
      longitude: -7.257664,
    },
    destination: {
      latitude: 30.3191841,
      longitude: -9.5072391,
    },
    result: 3675.1720647491934, //rounded to 3d.p
  },
  {
    origin: {
      latitude: 38.898556,
      longitude: -77.037852,
    },
    destination: {
      latitude: 38.897147,
      longitude: -77.043934,
    },
    result: 0.5491557912038084, //rounded to 3d.p
  },
]

describe('test distance function (Haversine formula)', () => {
  for (const obj of data) {
    it(`should return ${obj.result} for ${JSON.stringify(obj)}`, () => {
      const km = distance(obj.origin, obj.destination)
      expect(km).toEqual(obj.result)
    })
  }

  for (const customer of customersList) {
    it(`should match distance for customer "${customer.name}" from ${JSON.stringify(origin)}`, () => {
      const km = distance(origin, customer)
      expect(km).toMatchSnapshot()
    })
  }
})
