//@ts-nocheck
import parseRecord from '../src/parseRecord'

const validRecordJSON = '{"latitude": "52.986375", "user_id": 12, "name": "Christina McArdle", "longitude": "-6.043701"}'
const invalidValuesJSON = '{"latitude": "23bad latitude", "user_id": 1, "name": "Alice Cahill", "longitude": "-10.27699"}'
const missingFieldsJSON = '{"longitude": "-6.043701"}'
const badJSON = ',e": "-6.043701"}'
const empty = null

describe('parse json record', () => {
  it(`valid record - should return json for (${validRecordJSON})`, () => {
    const data = parseRecord(validRecordJSON)
    const json = JSON.parse(validRecordJSON)
    json.latitude = +json.latitude
    json.longitude = +json.longitude
    expect(data).toEqual(json)
  })

  it(`invalid values - should return null (${invalidValuesJSON})`, () => {
    const data = parseRecord(invalidValuesJSON)
    expect(data).toBe(null)
  })

  it(`missing fields - should return null (${missingFieldsJSON})`, () => {
    const data = parseRecord(missingFieldsJSON)
    expect(data).toBe(null)
  })

  it(`empty json - should return null`, () => {
    const data = parseRecord(empty)
    expect(data).toBe(null)
  })

  it(`fail silenty and return null for bad json`, () => {
    const data = parseRecord(badJSON)
    expect(data).toBe(null)
  })
})
