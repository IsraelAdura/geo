//@ts-nocheck
import {
  DEFAULT_ORIGIN_LATITUDE,
  DEFAULT_ORIGIN_LONGITUDE,
  MAX_ACCEPTED_DISTANCE_APART,
} from '../src/constants'

import processRecords from '../src/processRecords'
import { readFile } from 'fs/promises'

const files = ['customers', 'empty', 'missingValues', 'badLines', 'bad']
const coordinates = {
  latitude: DEFAULT_ORIGIN_LATITUDE,
  longitude: DEFAULT_ORIGIN_LONGITUDE,
} as const

describe('process records', () => {
  for (const file of files) {
    const fileName = `${file}.txt`
    it(`should process records for ${fileName}`, async () => {
      const data = await readFile(`tests/data/files/${fileName}`, 'utf8')
      const lines = typeof data === 'string' ? data.split('\n') : []
      const res = processRecords(
        lines,
        coordinates,
        MAX_ACCEPTED_DISTANCE_APART,
      )

      if (res.length) {
        expect(res).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              user_id: expect.any(Number),
              name: expect.any(String),
            }),
          ]),
        )
      }
      expect(res).toMatchSnapshot()
    })
  }
})
