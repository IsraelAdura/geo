# Geo 🌍

Outputs all customers within a 100km (default) radius from a given origin using the [Haversine](https://en.wikipedia.org/wiki/Great-circle_distance#Vector_version), great circle distance formula.


## Installation
Install Node and npm (or yarn)


Use the package manager [npm](https://www.npmjs.com/get-npm) or [yarn](https://classic.yarnpkg.com/en/docs/install/) to install packages.

```bash
npm install
```
or
```bash
yarn
```

## Built with
* Typescript

## How it works input
This app is structured to accept any JSON file and origin coordinates, also a link to a remote file can be passed as input.

it outputs the JSON objects of all customers within a given radius (defaults to 100km) sorted by user id in ascending order.

see [output](https://gitlab.com/IsraelAdura/geo/-/blob/master/output.txt) file
given the following [input](https://gitlab.com/IsraelAdura/geo/-/blob/master/tests/data/files/customers.txt)


## Errors and validation.
Each JSON line is validated to ensure all necessary parameters are present before it is considered, helpful error logs are also outputted to guide the user to failing lines.

## Usage
to run a demo test using the provided sample [data](https://s3.amazonaws.com/intercom-take-home-test/customers.txt)

```bash
npm run run-demo
```

## Testing
Tests are done with Jest, testing is a combination of snapshots and explicit assertions.
the test [fetch.test.ts](https://gitlab.com/IsraelAdura/geo/-/blob/master/tests/fetch.test.ts) requires internet connection to pass, so kindly ensure you are connected to the internet.

```bash
npm test
```

## License
[MIT](https://choosealicense.com/licenses/mit/)