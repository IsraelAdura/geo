import {
  DEFAULT_ORIGIN_LATITUDE,
  DEFAULT_ORIGIN_LONGITUDE,
} from '../src/constants'

import main from '../src/index'

const FILE_PATH = 'tests/data/files/customers.txt'

const coordinates = {
  latitude: DEFAULT_ORIGIN_LATITUDE,
  longitude: DEFAULT_ORIGIN_LONGITUDE,
} as const

const input = {
  path: FILE_PATH,
  // url: 'https://s3.amazonaws.com/intercom-take-home-test/customers.txt',
}

main(input, coordinates)
